package com.example.kitesurfingapp.network.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KitesurfingSpotFavoriteResp implements Serializable {

    @SerializedName("result")
    @Expose
    private String result;
    private final static long serialVersionUID = -5307958670702285942L;

    /**
     * No args constructor for use in serialization
     *
     */
    public KitesurfingSpotFavoriteResp() {
    }

    /**
     *
     * @param result
     */
    public KitesurfingSpotFavoriteResp(String result) {
        super();
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
