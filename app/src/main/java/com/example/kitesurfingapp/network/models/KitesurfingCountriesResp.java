package com.example.kitesurfingapp.network.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KitesurfingCountriesResp implements Serializable {

    @SerializedName("result")
    @Expose
    private List<String> result = null;
    private final static long serialVersionUID = -4381711506833106932L;

    /**
     * No args constructor for use in serialization
     *
     */
    public KitesurfingCountriesResp() {
    }

    /**
     *
     * @param result
     */
    public KitesurfingCountriesResp(List<String> result) {
        super();
        this.result = result;
    }

    public List<String> getResult() {
        return result;
    }

    public void setResult(List<String> result) {
        this.result = result;
    }

}
