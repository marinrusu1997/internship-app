package com.example.kitesurfingapp.network.models.errors;

import java.io.Serializable;

import com.example.kitesurfingapp.network.models.errors.common.SentHeaders;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.example.kitesurfingapp.network.models.errors.common.Error;

public class GetKitesurfingSpotDetailsError implements Serializable {

    public class SentBodyParameters implements Serializable {

        @SerializedName("spotId")
        @Expose
        private String spotId;
        private final static long serialVersionUID = 743674225454934783L;

        /**
         * No args constructor for use in serialization
         *
         */
        public SentBodyParameters() {
        }

        /**
         *
         * @param spotId
         */
        public SentBodyParameters(String spotId) {
            super();
            this.spotId = spotId;
        }

        public String getSpotId() {
            return spotId;
        }

        public void setSpotId(String spotId) {
            this.spotId = spotId;
        }

    }

    @SerializedName("error")
    @Expose
    private Error error;
    @SerializedName("sentBodyParameters")
    @Expose
    private SentBodyParameters sentBodyParameters;
    @SerializedName("sentHeaders")
    @Expose
    private SentHeaders sentHeaders;
    private final static long serialVersionUID = -6786901419162234034L;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetKitesurfingSpotDetailsError() {
    }

    /**
     *
     * @param error
     * @param sentBodyParameters
     * @param sentHeaders
     */
    public GetKitesurfingSpotDetailsError(Error error, SentBodyParameters sentBodyParameters, SentHeaders sentHeaders) {
        super();
        this.error = error;
        this.sentBodyParameters = sentBodyParameters;
        this.sentHeaders = sentHeaders;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public SentBodyParameters getSentBodyParameters() {
        return sentBodyParameters;
    }

    public void setSentBodyParameters(SentBodyParameters sentBodyParameters) {
        this.sentBodyParameters = sentBodyParameters;
    }

    public SentHeaders getSentHeaders() {
        return sentHeaders;
    }

    public void setSentHeaders(SentHeaders sentHeaders) {
        this.sentHeaders = sentHeaders;
    }

}
