package com.example.kitesurfingapp.network.models.errors;

import java.io.Serializable;

import com.example.kitesurfingapp.network.models.errors.common.SentHeaders;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.example.kitesurfingapp.network.models.errors.common.Error;

public class GetAuthenticationTokenError implements Serializable {

    public class SentBodyParameters implements Serializable {

        @SerializedName("email")
        @Expose
        private String email;
        private final static long serialVersionUID = 3141823355820958465L;

        /**
         * No args constructor for use in serialization
         *
         */
        public SentBodyParameters() {
        }

        /**
         *
         * @param email
         */
        public SentBodyParameters(String email) {
            super();
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

    }

    @SerializedName("error")
    @Expose
    private Error error;
    @SerializedName("sentBodyParameters")
    @Expose
    private SentBodyParameters sentBodyParameters;
    @SerializedName("sentHeaders")
    @Expose
    private SentHeaders sentHeaders;
    private final static long serialVersionUID = -4124708684158216428L;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetAuthenticationTokenError() {
    }

    /**
     *
     * @param error
     * @param sentBodyParameters
     * @param sentHeaders
     */
    public GetAuthenticationTokenError(Error error, SentBodyParameters sentBodyParameters, SentHeaders sentHeaders) {
        super();
        this.error = error;
        this.sentBodyParameters = sentBodyParameters;
        this.sentHeaders = sentHeaders;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public SentBodyParameters getSentBodyParameters() {
        return sentBodyParameters;
    }

    public void setSentBodyParameters(SentBodyParameters sentBodyParameters) {
        this.sentBodyParameters = sentBodyParameters;
    }

    public SentHeaders getSentHeaders() {
        return sentHeaders;
    }

    public void setSentHeaders(SentHeaders sentHeaders) {
        this.sentHeaders = sentHeaders;
    }

}
