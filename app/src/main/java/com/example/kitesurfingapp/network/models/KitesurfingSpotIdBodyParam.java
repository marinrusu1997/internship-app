package com.example.kitesurfingapp.network.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KitesurfingSpotIdBodyParam implements Serializable {

    @SerializedName("spotId")
    @Expose
    private String spotId;
    private final static long serialVersionUID = -7793591606868459656L;

    /**
     * No args constructor for use in serialization
     *
     */
    public KitesurfingSpotIdBodyParam() {
    }

    /**
     *
     * @param spotId
     */
    public KitesurfingSpotIdBodyParam(String spotId) {
        super();
        this.spotId = spotId;
    }

    public String getSpotId() {
        return spotId;
    }

    public void setSpotId(String spotId) {
        this.spotId = spotId;
    }

}
