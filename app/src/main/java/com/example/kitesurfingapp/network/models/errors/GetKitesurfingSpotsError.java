package com.example.kitesurfingapp.network.models.errors;

import java.io.Serializable;

import com.example.kitesurfingapp.network.models.errors.common.Error;
import com.example.kitesurfingapp.network.models.errors.common.SentHeaders;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetKitesurfingSpotsError implements Serializable {

    public class SentBodyParameters implements Serializable {

        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("windProbability")
        @Expose
        private String windProbability;
        private final static long serialVersionUID = 4461469059154935406L;

        /**
         * No args constructor for use in serialization
         *
         */
        public SentBodyParameters() {
        }

        /**
         *
         * @param windProbability
         * @param country
         */
        public SentBodyParameters(String country, String windProbability) {
            super();
            this.country = country;
            this.windProbability = windProbability;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getWindProbability() {
            return windProbability;
        }

        public void setWindProbability(String windProbability) {
            this.windProbability = windProbability;
        }

    }

    @SerializedName("error")
    @Expose
    private Error error;
    @SerializedName("sentBodyParameters")
    @Expose
    private SentBodyParameters sentBodyParameters;
    @SerializedName("sentHeaders")
    @Expose
    private SentHeaders sentHeaders;
    private final static long serialVersionUID = -2397358232092769212L;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetKitesurfingSpotsError() {
    }

    /**
     *
     * @param error
     * @param sentBodyParameters
     * @param sentHeaders
     */
    public GetKitesurfingSpotsError(Error error, SentBodyParameters sentBodyParameters, SentHeaders sentHeaders) {
        super();
        this.error = error;
        this.sentBodyParameters = sentBodyParameters;
        this.sentHeaders = sentHeaders;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public SentBodyParameters getSentBodyParameters() {
        return sentBodyParameters;
    }

    public void setSentBodyParameters(SentBodyParameters sentBodyParameters) {
        this.sentBodyParameters = sentBodyParameters;
    }

    public SentHeaders getSentHeaders() {
        return sentHeaders;
    }

    public void setSentHeaders(SentHeaders sentHeaders) {
        this.sentHeaders = sentHeaders;
    }

}


