package com.example.kitesurfingapp.network.models;

import java.io.Serializable;
import java.util.List;

import com.example.kitesurfingapp.models.KitesurfingSpot;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KitesurfingSpotPlacesList implements Serializable
{
    public static class Filter implements Serializable {

        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("windProbability")
        @Expose
        private String windProbability;
        private final static long serialVersionUID = -3512140823874482914L;

        /**
         * No args constructor for use in serialization
         *
         */
        public Filter() {
            super();
            this.country = "";
            this.windProbability = "";
        }

        /**
         *
         * @param windProbability
         * @param country
         */
        public Filter(String country, String windProbability) {
            super();
            this.country = country;
            this.windProbability = windProbability;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getWindProbability() {
            return windProbability;
        }

        public void setWindProbability(String windProbability) {
            this.windProbability = windProbability;
        }

    }

    @SerializedName("result")
    @Expose
    private List<KitesurfingSpot> result = null;
    private final static long serialVersionUID = 6594044503568578693L;

    /**
     * No args constructor for use in serialization
     *
     */
    public KitesurfingSpotPlacesList() {
    }

    /**
     *
     * @param result
     */
    public KitesurfingSpotPlacesList(List<KitesurfingSpot> result) {
        super();
        this.result = result;
    }

    public List<KitesurfingSpot> getKitesurfingSpotPlaces() {
        return result;
    }

    public void setKitesurfingSpotPlaces(List<KitesurfingSpot> result) {
        this.result = result;
    }

   /* public List<KitesurfingSpot> getKitesurfingSpotPlaces() {
        List<KitesurfingSpot> places = new LinkedList<>();
        for(Result res : this.result) {
            places.add(new KitesurfingSpot(
                    res.getId(), res.getName(),
                    res.getCountry(), res.getWhenToGo(),
                    res.isFavorite
            ));
        }
        return places;
    }
    */
}




