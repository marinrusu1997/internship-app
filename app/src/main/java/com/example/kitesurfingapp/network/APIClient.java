package com.example.kitesurfingapp.network;

import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static Retrofit retrofit = null;
    private static String accesTokenValue = "";
    private static String BASE_URL = "https://internship-2019.herokuapp.com/";

    public static void setAccesTokenValue(String tokenValue) {
        assert (tokenValue != null && !tokenValue.isEmpty());
        accesTokenValue = tokenValue;
    }

    public static Retrofit getClient() {
        return retrofit;
    }

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        return chain.proceed(chain.request().newBuilder()
                                .addHeader("Content-Type","application/json")
                                .addHeader("token", accesTokenValue)
                                .build()
                        );
                    }
                })
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .client(client)
                .build();
    }
}
