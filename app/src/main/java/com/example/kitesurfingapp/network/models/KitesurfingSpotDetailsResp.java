package com.example.kitesurfingapp.network.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KitesurfingSpotDetailsResp implements Serializable
{
    public class Result implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("longitude")
        @Expose
        private Double longitude;
        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("windProbability")
        @Expose
        private Integer windProbability;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("whenToGo")
        @Expose
        private String whenToGo;
        @SerializedName("isFavorite")
        @Expose
        private Boolean isFavorite;
        private final static long serialVersionUID = -2806106649153608719L;

        /**
         * No args constructor for use in serialization
         *
         */
        public Result() {
        }

        /**
         *
         * @param windProbability
         * @param id
         * @param whenToGo
         * @param isFavorite
         * @param name
         * @param longitude
         * @param latitude
         * @param country
         */
        public Result(String id, String name, Double longitude, Double latitude, Integer windProbability, String country, String whenToGo, Boolean isFavorite) {
            super();
            this.id = id;
            this.name = name;
            this.longitude = longitude;
            this.latitude = latitude;
            this.windProbability = windProbability;
            this.country = country;
            this.whenToGo = whenToGo;
            this.isFavorite = isFavorite;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Integer getWindProbability() {
            return windProbability;
        }

        public void setWindProbability(Integer windProbability) {
            this.windProbability = windProbability;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getWhenToGo() {
            return whenToGo;
        }

        public void setWhenToGo(String whenToGo) {
            this.whenToGo = whenToGo;
        }

        public Boolean getIsFavorite() {
            return isFavorite;
        }

        public void setIsFavorite(Boolean isFavorite) {
            this.isFavorite = isFavorite;
        }

    }

    @SerializedName("result")
    @Expose
    private Result result;
    private final static long serialVersionUID = -4538328789846053280L;

    /**
     * No args constructor for use in serialization
     *
     */
    public KitesurfingSpotDetailsResp() {
    }

    /**
     *
     * @param result
     */
    public KitesurfingSpotDetailsResp(Result result) {
        super();
        this.result = result;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

}
