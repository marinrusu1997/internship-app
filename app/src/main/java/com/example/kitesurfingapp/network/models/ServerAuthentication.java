package com.example.kitesurfingapp.network.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServerAuthentication implements Serializable {

    public static class Email implements Serializable {

        @SerializedName("email")
        @Expose
        private String email;
        private final static long serialVersionUID = 1929931733200676263L;

        /**
         * No args constructor for use in serialization
         *
         */
        public Email() {
        }

        /**
         *
         * @param email
         */
        public Email(String email) {
            super();
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    private static class Result implements Serializable {

        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("email")
        @Expose
        private String email;
        private final static long serialVersionUID = 2246929960961319089L;

        /**
         * No args constructor for use in serialization
         *
         */
        public Result() {
        }

        /**
         *
         * @param email
         * @param token
         */
        public Result(String token, String email) {
            super();
            this.token = token;
            this.email = email;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    @SerializedName("result")
    @Expose
    private Result result;
    private final static long serialVersionUID = -3693555713383518152L;

    /**
     * No args constructor for use in serialization
     *
     */
    public ServerAuthentication() {
    }

    public String getToken() {
        return result.getToken();
    }

    public String getEmail() {
        return result.getEmail();
    }

}

