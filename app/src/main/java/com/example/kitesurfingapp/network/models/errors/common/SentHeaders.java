package com.example.kitesurfingapp.network.models.errors.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SentHeaders implements Serializable
{

    @SerializedName("content-type")
    @Expose
    private String contentType;
    @SerializedName("token")
    @Expose
    private String token;
    private final static long serialVersionUID = -7737771486344785190L;

    /**
     * No args constructor for use in serialization
     *
     */
    public SentHeaders() {
    }

    /**
     *
     * @param token
     * @param contentType
     */
    public SentHeaders(String contentType, String token) {
        super();
        this.contentType = contentType;
        this.token = token;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
