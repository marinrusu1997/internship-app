package com.example.kitesurfingapp.network;

import com.example.kitesurfingapp.network.models.KitesurfingSpotFavoriteResp;
import com.example.kitesurfingapp.network.models.KitesurfingCountriesResp;
import com.example.kitesurfingapp.network.models.KitesurfingSpotDetailsResp;
import com.example.kitesurfingapp.network.models.KitesurfingSpotIdBodyParam;
import com.example.kitesurfingapp.network.models.KitesurfingSpotPlacesList;
import com.example.kitesurfingapp.network.models.ServerAuthentication;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIInterface {

    @POST("/api-user-get")
    Call<ServerAuthentication> doGetAuthenticationForUser(@Body ServerAuthentication.Email email);

    @POST("/api-spot-get-all")
    Call<KitesurfingSpotPlacesList> doGetAllSpotPlaces(@Body KitesurfingSpotPlacesList.Filter filter);

    @POST("/api-spot-favorites-add")
    Call<KitesurfingSpotFavoriteResp> doAddSpotToFavorites(@Body KitesurfingSpotIdBodyParam id);

    @POST("/api-spot-favorites-remove")
    Call<KitesurfingSpotFavoriteResp> doRemoveSpotFromFavorites(@Body KitesurfingSpotIdBodyParam id);

    @POST("/api-spot-get-details")
    Call<KitesurfingSpotDetailsResp> doGetKitesurfingSpotDetails(@Body KitesurfingSpotIdBodyParam id);

    @POST("/api-spot-get-countries")
    Call<KitesurfingCountriesResp> doGetKitesurfingCountries();
}
