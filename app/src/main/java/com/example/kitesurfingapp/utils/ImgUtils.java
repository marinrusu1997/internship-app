package com.example.kitesurfingapp.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;


public class ImgUtils {

    public static Drawable changeColor(@NonNull Context context, @NonNull Drawable drawable, int colorResourceId) {
        DrawableCompat.setTint(drawable, ContextCompat.getColor(context, colorResourceId));
        return drawable;
    }
}
