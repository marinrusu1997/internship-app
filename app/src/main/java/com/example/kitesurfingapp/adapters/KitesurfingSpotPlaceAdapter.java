package com.example.kitesurfingapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kitesurfingapp.R;
import com.example.kitesurfingapp.models.KitesurfingSpot;
import com.example.kitesurfingapp.utils.ImgUtils;

import java.util.List;

public class KitesurfingSpotPlaceAdapter extends ArrayAdapter<KitesurfingSpot> {
    public interface OnStarToglingCallback {
        void onTogle(KitesurfingSpot place);
    }

    private OnStarToglingCallback onStarToglingCallback = null;

    public KitesurfingSpotPlaceAdapter(@NonNull Context context, int resource, @NonNull List<KitesurfingSpot> objects) {
        super(context, resource, objects);
    }

    public void setOnStarToglingCallback(OnStarToglingCallback callback) {
        this.onStarToglingCallback = callback;
    }

    public void changeIsFavorite(String id, boolean isFavorite) {
        int count = getCount();
        KitesurfingSpot place = null;
        KitesurfingSpot temp = null;
        for(int i = 0; i < count; i++) {
            temp = getItem(i);
            if (temp.getId().equals(id)) {
                place = temp;
                break;
            }
        }
        if (place == null)
            throw new IllegalArgumentException("id is not valid, element not found");
        place.setIsFavorite(isFavorite);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Context context = getContext();
        convertView = convertView == null
                ? LayoutInflater.from(context).inflate(R.layout.kitesurfing_spot_place,null)
                : convertView;

        final KitesurfingSpot kitesurfingSpot = getItem(position);

        final ImageView starImg = convertView.findViewById(R.id.kitesurfingSpotPlaceStarImageView);

        starImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kitesurfingSpot.toggleFavorite();
                if (onStarToglingCallback != null)
                    onStarToglingCallback.onTogle(kitesurfingSpot);
                notifyDataSetChanged();
            }
        });

        starImg.setImageDrawable(kitesurfingSpot.isFavorite()
            ? context.getDrawable(R.mipmap.star_on)
            : ImgUtils.changeColor(context,
                context.getDrawable(R.mipmap.star_off), R.color.colorYellow));

        ((TextView)convertView.findViewById(R.id.kitesurfingSpotPlaceCityName))
                .setText(kitesurfingSpot.getName());
        ((TextView)convertView.findViewById(R.id.kitesurfingSpotPlaceCountryName))
                .setText(kitesurfingSpot.getCountry());

        return convertView;
    }
}
