package com.example.kitesurfingapp.activities;


import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import com.example.kitesurfingapp.R;
import com.example.kitesurfingapp.adapters.KitesurfingSpotPlaceAdapter;
import com.example.kitesurfingapp.data_access_layer.KitesurfingSpotsDataProvider;
import com.example.kitesurfingapp.models.KitesurfingSpot;

import com.example.kitesurfingapp.network.models.KitesurfingSpotDetailsResp;
import com.example.kitesurfingapp.network.models.KitesurfingSpotIdBodyParam;
import com.example.kitesurfingapp.network.models.KitesurfingSpotPlacesList;
import com.example.kitesurfingapp.utils.ImgUtils;

import java.util.LinkedList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private static final String USER_EMAIL = "dimarusu2000@gmail.com";
    private static final String KITESURFING_SPOT_LIST_SERIALIZABLE_KEY = "KITESURFING_SPOT_LIST_SERIALIZABLE_KEY";
    private static final Integer DISPLAY_KITESURFING_SPOT_DETAILS_REQ_CODE = 0;
    private static final Integer KITESURFING_FILTERING_REQ_CODE = 1;

    private ListView lvKitesurfingSpotPlaces = null;
    private KitesurfingSpotsDataProvider kitesurfingSpotsDataProvider = null;
    private KitesurfingSpotPlaceAdapter kitesurfingSpotPlaceAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        doInitializations();
        doGetDataForListView(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        LinkedList<KitesurfingSpot> kitesurfingSpots = new LinkedList<>();
        int count = kitesurfingSpotPlaceAdapter.getCount();
        for(int i = 0; i < count; i++)
            kitesurfingSpots.add(kitesurfingSpotPlaceAdapter.getItem(i));
        outState.putSerializable(KITESURFING_SPOT_LIST_SERIALIZABLE_KEY, kitesurfingSpots);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mainActivityMenu = getMenuInflater();
        mainActivityMenu.inflate(R.menu.main_activity_menu, menu);

        MenuItem filterMenuItem = menu.findItem(R.id.action_filter_spots);
        filterMenuItem.setIcon(ImgUtils.changeColor(getApplicationContext(),
                filterMenuItem.getIcon(), R.color.colorWhite));

        return super.onCreateOptionsMenu(menu);
    }

    private void doInitializations() {
        kitesurfingSpotsDataProvider = new KitesurfingSpotsDataProvider(getApplicationContext(), USER_EMAIL);

        lvKitesurfingSpotPlaces = findViewById(R.id.lvKitesurfingSpotPlaces);

        kitesurfingSpotPlaceAdapter = new KitesurfingSpotPlaceAdapter(getApplicationContext(),
                R.layout.support_simple_spinner_dropdown_item,
                new LinkedList<KitesurfingSpot>());
        kitesurfingSpotPlaceAdapter.setOnStarToglingCallback(new KitesurfingSpotPlaceAdapter.OnStarToglingCallback() {
            @Override
            public void onTogle(final KitesurfingSpot place) {
                if (place.isFavorite())
                    kitesurfingSpotsDataProvider.addKitesurfingSpotToFavorites(new KitesurfingSpotIdBodyParam(place.getId()),
                            new KitesurfingSpotsDataProvider.KitesurfingSpotToAndFromFavCallback() {
                                @Override
                                public void onSucces(String id) {

                                }
                                @Override
                                public void onError(String msg) {
                                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    /* undo last togling */
                                    kitesurfingSpotPlaceAdapter.changeIsFavorite(place.getId(), !place.getIsFavorite());
                                }
                            });
                else
                    kitesurfingSpotsDataProvider.removeKitesurfingSpotFromFavorites(new KitesurfingSpotIdBodyParam(place.getId()),
                            new KitesurfingSpotsDataProvider.KitesurfingSpotToAndFromFavCallback() {
                                @Override
                                public void onSucces(String id) {

                                }
                                @Override
                                public void onError(String msg) {
                                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    /* undo last togling */
                                    kitesurfingSpotPlaceAdapter.changeIsFavorite(place.getId(), !place.getIsFavorite());
                                }
                    });
            }
        });

        lvKitesurfingSpotPlaces.setAdapter(kitesurfingSpotPlaceAdapter);

        lvKitesurfingSpotPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                kitesurfingSpotsDataProvider.getKitesurfingSpotDetails(
                        new KitesurfingSpotIdBodyParam(kitesurfingSpotPlaceAdapter.getItem(position).getId()),
                        new KitesurfingSpotsDataProvider.OnKitesurfingSpotDetailsCallback() {
                            @Override
                            public void onSuccess(KitesurfingSpotDetailsResp resp) {
                                Intent intent = new Intent(getApplicationContext(), KitesurfingSpotDetailsActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(ActivitiesDataPassing.KITESURFING_SPOT_DETAILS_SERIALIZABLE_KEY,
                                        resp);
                                intent.putExtras(bundle);
                                startActivityForResult(intent, DISPLAY_KITESURFING_SPOT_DETAILS_REQ_CODE);
                            }

                            @Override
                            public void onFailure(String msg) {
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }

    public void doGetDataForListView(Bundle savedInstanceState) {
        if (savedInstanceState != null) { /* cached data is much cheaper */
            List<KitesurfingSpot> spots = (List)savedInstanceState
                    .getSerializable(KITESURFING_SPOT_LIST_SERIALIZABLE_KEY);
            if (spots != null) {
                kitesurfingSpotPlaceAdapter.addAll();
                kitesurfingSpotPlaceAdapter.notifyDataSetChanged();
            }
        } else {
            kitesurfingSpotsDataProvider.getKitesurfingSpotPlaces(new KitesurfingSpotPlacesList.Filter(),
                    new KitesurfingSpotsDataProvider.GetKitesurfingSpotPlacesCallback() {
                        @Override
                        public void onKitesurfingSpotDatasetReady(List<KitesurfingSpot> places) {
                            kitesurfingSpotPlaceAdapter.addAll(places);
                            kitesurfingSpotPlaceAdapter.notifyDataSetChanged();
                        }
                    });
        }
    }

    @Override
    protected void onDestroy() {
        kitesurfingSpotsDataProvider.close();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter_spots: {
                startActivityForResult(new Intent(getApplicationContext(), KitesurfingFilterActivity.class),
                        KITESURFING_FILTERING_REQ_CODE);
            }
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == DISPLAY_KITESURFING_SPOT_DETAILS_REQ_CODE && resultCode == RESULT_OK && data != null) {
            final KitesurfingSpotDetailsResp.Result result = ((KitesurfingSpotDetailsResp) data.getExtras()
                    .getSerializable(ActivitiesDataPassing.KITESURFING_SPOT_DETAILS_SERIALIZABLE_KEY)).getResult();
            if (result.getIsFavorite() == true) {
                kitesurfingSpotsDataProvider.addKitesurfingSpotToFavorites(new KitesurfingSpotIdBodyParam(result.getId()),
                        new KitesurfingSpotsDataProvider.KitesurfingSpotToAndFromFavCallback() {
                            @Override
                            public void onSucces(String id) {
                                kitesurfingSpotPlaceAdapter.changeIsFavorite(result.getId(), result.getIsFavorite());
                            }

                            @Override
                            public void onError(String msg) {
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        });
            } else {
                kitesurfingSpotsDataProvider.removeKitesurfingSpotFromFavorites(new KitesurfingSpotIdBodyParam(result.getId()),
                        new KitesurfingSpotsDataProvider.KitesurfingSpotToAndFromFavCallback() {
                            @Override
                            public void onSucces(String id) {
                                kitesurfingSpotPlaceAdapter.changeIsFavorite(result.getId(), result.getIsFavorite());
                            }

                            @Override
                            public void onError(String msg) {
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }
        if (requestCode == KITESURFING_FILTERING_REQ_CODE && resultCode == RESULT_OK && data != null) {
            ActivitiesDataPassing.KitesurfingFilter filter = (ActivitiesDataPassing.KitesurfingFilter) data.getExtras()
                    .getSerializable(ActivitiesDataPassing.KITESURFING_FILTER_SERIALIZABLE_KEY);
            if (!filter.country.isEmpty() && !kitesurfingSpotsDataProvider.isCountryValid(filter.country)) {
                Toast.makeText(getApplicationContext(), "Country is invalid", Toast.LENGTH_SHORT).show();
                return;
            }
            kitesurfingSpotsDataProvider.getKitesurfingSpotPlaces(new KitesurfingSpotPlacesList.Filter(filter.country,
                            filter.windProbability == 0 ? "" : filter.windProbability.toString()),
                    new KitesurfingSpotsDataProvider.GetKitesurfingSpotPlacesCallback() {
                        @Override
                        public void onKitesurfingSpotDatasetReady(List<KitesurfingSpot> places) {
                            kitesurfingSpotPlaceAdapter.clear();
                            kitesurfingSpotPlaceAdapter.addAll(places);
                            kitesurfingSpotPlaceAdapter.notifyDataSetChanged();
                        }
            });
        }
    }
}
