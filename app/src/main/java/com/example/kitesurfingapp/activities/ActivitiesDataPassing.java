package com.example.kitesurfingapp.activities;


import java.io.Serializable;

public abstract class ActivitiesDataPassing {
    private ActivitiesDataPassing() {}

    public static final String KITESURFING_SPOT_DETAILS_SERIALIZABLE_KEY = "KITESURFING_SPOT_DETAILS_SERIALIZABLE_KEY";
    public static final String KITESURFING_FILTER_SERIALIZABLE_KEY = "KITESURFING_FILTER_SERIALIZABLE_KEY";

    public static final class KitesurfingFilter implements Serializable {
        public String country;
        public Integer windProbability;

        public KitesurfingFilter(String country, Integer windProbability) {
            this.country = country;
            this.windProbability = windProbability;
        }
    }
}
