package com.example.kitesurfingapp.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.kitesurfingapp.R;
import com.example.kitesurfingapp.network.models.KitesurfingSpotDetailsResp;
import com.example.kitesurfingapp.utils.ImgUtils;

import org.apache.commons.lang3.StringUtils;

public class KitesurfingSpotDetailsActivity extends AppCompatActivity {

    private boolean isFavoriteInitialState;
    private KitesurfingSpotDetailsResp spotDetails = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitesurfing_spot_details);

        tryDisplayKitesurfingSpot();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void tryDisplayKitesurfingSpot() {
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                spotDetails = ((KitesurfingSpotDetailsResp) bundle
                        .getSerializable(ActivitiesDataPassing.KITESURFING_SPOT_DETAILS_SERIALIZABLE_KEY));
                KitesurfingSpotDetailsResp.Result result = spotDetails.getResult();

                setTitle(result.getName());
                ((TextView)findViewById(R.id.tv_country_display))
                        .setText(result.getCountry());
                ((TextView)findViewById(R.id.tv_latitude_display))
                        .setText(result.getLatitude().toString());
                ((TextView)findViewById(R.id.tv_longitude_display))
                        .setText(result.getLongitude().toString());
                ((TextView)findViewById(R.id.tv_when_to_go_display))
                        .setText(StringUtils.capitalize(result.getWhenToGo().toLowerCase()));
                ((TextView)findViewById(R.id.tv_windprob_display))
                        .setText(result.getWindProbability().toString());

                isFavoriteInitialState = result.getIsFavorite();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mainActivityMenu = getMenuInflater();
        mainActivityMenu.inflate(R.menu.kitesurfing_spot_display_act_menu, menu);

        MenuItem togleFavMenuItem = menu.findItem(R.id.action_make_fav);
        togleFavMenuItem.setIcon(spotDetails.getResult().getIsFavorite() ? getDrawable(R.mipmap.star_on)
                : ImgUtils.changeColor(getApplicationContext(), getDrawable(R.mipmap.star_off), R.color.colorWhite));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        KitesurfingSpotDetailsResp.Result result = spotDetails.getResult();

        switch (item.getItemId()) {
            case R.id.action_make_fav: {
                result.setIsFavorite(!result.getIsFavorite());
                item.setIcon(result.getIsFavorite() ? getDrawable(R.mipmap.star_on)
                        : ImgUtils.changeColor(getApplicationContext(), getDrawable(R.mipmap.star_off), R.color.colorWhite));
            }
            break;
            case android.R.id.home: {
                finish();
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        if (spotDetails != null && spotDetails.getResult().getIsFavorite() != isFavoriteInitialState) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable(ActivitiesDataPassing.KITESURFING_SPOT_DETAILS_SERIALIZABLE_KEY, spotDetails);
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_CANCELED);
        }

        super.finish();
    }

}
