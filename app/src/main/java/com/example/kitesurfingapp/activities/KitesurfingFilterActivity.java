package com.example.kitesurfingapp.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.kitesurfingapp.R;

public class KitesurfingFilterActivity extends AppCompatActivity {

    private EditText etCountry = null;
    private EditText etWindProb = null;
    private Button btnApply = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitesurfing_filter);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        setTitle(R.string.kitesurfing_filter_activity_name);

        doInitializations();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void doInitializations() {
        etCountry = findViewById(R.id.et_country_filter);
        etWindProb = findViewById(R.id.et_wind_prob_filter);
        btnApply = findViewById(R.id.btn_apply_filter);

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String country = etCountry.getText().toString();
                Integer windProb = 0;
                try {
                     windProb = Integer.parseInt(etWindProb.getText().toString());
                } catch (NumberFormatException e) {
                }
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable(ActivitiesDataPassing.KITESURFING_FILTER_SERIALIZABLE_KEY,
                        new ActivitiesDataPassing.KitesurfingFilter(country,windProb));
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
