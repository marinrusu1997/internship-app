package com.example.kitesurfingapp.models;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class KitesurfingSpot implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("whenToGo")
    @Expose
    private String whenToGo;
    @SerializedName("isFavorite")
    @Expose
    private Boolean isFavorite;
    private final static long serialVersionUID = 2655266893332628469L;

    /**
     * No args constructor for use in serialization
     *
     */
    public KitesurfingSpot() {
    }

    /**
     *
     * @param id
     * @param whenToGo
     * @param isFavorite
     * @param name
     * @param country
     */
    public KitesurfingSpot(String id, String name, String country, String whenToGo, Boolean isFavorite) {
        super();
        this.id = id;
        this.name = name;
        this.country = country;
        this.whenToGo = whenToGo;
        this.isFavorite = isFavorite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWhenToGo() {
        return whenToGo;
    }

    public void setWhenToGo(String whenToGo) {
        this.whenToGo = whenToGo;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public void toggleFavorite() {
        this.isFavorite = !this.isFavorite;
    }

    public boolean isFavorite() {
        return this.isFavorite == true;
    }

}