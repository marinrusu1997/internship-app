package com.example.kitesurfingapp.data_access_layer.database;

import android.provider.BaseColumns;

public class KitesurfingSpotContract {
    private KitesurfingSpotContract() {}

    public static class KitesurfingSpotEntry implements BaseColumns {
        public static final String TABLE_NAME = "kitesurfingspots";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_COUNTRY = "country";
        public static final String COLUMN_NAME_WHEN_TO_GO = "whentogo";
        public static final String COLUMN_NAME_IS_FAVORITE = "isfavorite";
        public static final String COLUMN_NAME_IS_DIRTY = "isdirty";

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + KitesurfingSpotEntry.TABLE_NAME + " (" +
                        KitesurfingSpotEntry.COLUMN_NAME_ID + " TEXT PRIMARY KEY," +
                        KitesurfingSpotEntry.COLUMN_NAME_NAME + " TEXT," +
                        KitesurfingSpotEntry.COLUMN_NAME_COUNTRY + " TEXT," +
                        KitesurfingSpotEntry.COLUMN_NAME_WHEN_TO_GO + " TEXT," +
                        KitesurfingSpotEntry.COLUMN_NAME_IS_FAVORITE + " INTEGER, " +
                        KitesurfingSpotEntry.COLUMN_NAME_IS_DIRTY + " INTEGER)";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + KitesurfingSpotEntry.TABLE_NAME;
    }
}
