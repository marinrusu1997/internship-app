package com.example.kitesurfingapp.data_access_layer;

import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.kitesurfingapp.data_access_layer.database.KitesurfingSpotDatabase;
import com.example.kitesurfingapp.models.KitesurfingSpot;
import com.example.kitesurfingapp.network.APIClient;
import com.example.kitesurfingapp.network.APIInterface;
import com.example.kitesurfingapp.network.models.KitesurfingSpotFavoriteResp;
import com.example.kitesurfingapp.network.models.KitesurfingCountriesResp;
import com.example.kitesurfingapp.network.models.KitesurfingSpotDetailsResp;
import com.example.kitesurfingapp.network.models.KitesurfingSpotIdBodyParam;
import com.example.kitesurfingapp.network.models.KitesurfingSpotPlacesList;
import com.example.kitesurfingapp.network.models.ServerAuthentication;
import com.example.kitesurfingapp.network.models.errors.AddKitesurfingSpotToFavError;
import com.example.kitesurfingapp.network.models.errors.GetAuthenticationTokenError;
import com.example.kitesurfingapp.network.models.errors.GetKitesurfingSpotDetailsError;
import com.example.kitesurfingapp.network.models.errors.GetKitesurfingSpotsError;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/* This class encapsulates all I/O logic */
/* For clients it's transparent which I/O is using, socket or database */
public class KitesurfingSpotsDataProvider implements AutoCloseable {

    public interface GetKitesurfingSpotPlacesCallback {
        void onKitesurfingSpotDatasetReady(List<KitesurfingSpot> places);
    }

    public interface KitesurfingSpotToAndFromFavCallback {
        void onSucces(String id);

        void onError(String msg);
    }

    public interface OnKitesurfingSpotDetailsCallback {
        void onSuccess(KitesurfingSpotDetailsResp resp);

        void onFailure(String msg);
    }

    private static final String DATA_PROVIDER_LOG_TAG = "DATA_PROVIDER_LOG_TAG";

    private APIInterface apiInterface;
    private List<String> kitesurfingCountries = null;
    private boolean isDataFetchedAlready = false;

    public KitesurfingSpotsDataProvider(Context context, final String userEmail) {
        apiInterface = APIClient.getClient().create(APIInterface.class);
        KitesurfingSpotDatabase.init(context);
        doSynchronizedInitializations(userEmail);
        doGetKitesurfingCountriesAsync();
    }

    public void getKitesurfingSpotPlaces(KitesurfingSpotPlacesList.Filter filter, @NonNull final GetKitesurfingSpotPlacesCallback callback) {
        apiInterface.doGetAllSpotPlaces(filter).enqueue(new Callback<KitesurfingSpotPlacesList>() {
            @Override
            public void onResponse(Call<KitesurfingSpotPlacesList> call, Response<KitesurfingSpotPlacesList> response) {
                try {
                    if (response.isSuccessful()) {
                        List<KitesurfingSpot> places = response.body().getKitesurfingSpotPlaces();
                        callback.onKitesurfingSpotDatasetReady(places);
                        isDataFetchedAlready = true; /* must be set before we write to db */
                        KitesurfingSpotDatabase.dropDatabase();
                        KitesurfingSpotDatabase.write(places);
                        return; /* we are done */
                    } else {
                        GetKitesurfingSpotsError error = (GetKitesurfingSpotsError)
                                APIClient.getClient().responseBodyConverter(GetKitesurfingSpotsError.class, new Annotation[0])
                                        .convert(response.errorBody());
                        Log.d(DATA_PROVIDER_LOG_TAG, error.getError().getMessage() + " " + error.getError().getCode());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(DATA_PROVIDER_LOG_TAG, e.getMessage());
                }

                /*
                 *  If we are here, then we got server or connection error
                 *  or there was db exception, it won't be counted anyway because of flag
                 */
                if (!isDataFetchedAlready) {
                    callback.onKitesurfingSpotDatasetReady(KitesurfingSpotDatabase.readAll());
                    isDataFetchedAlready = true;
                }
            }

            @Override
            public void onFailure(Call<KitesurfingSpotPlacesList> call, Throwable t) {
                call.cancel();
                Log.d(DATA_PROVIDER_LOG_TAG, t.getMessage());
                /* Need cached data */
                if (!isDataFetchedAlready) {
                    callback.onKitesurfingSpotDatasetReady(KitesurfingSpotDatabase.readAll());
                    isDataFetchedAlready = true;
                }
            }
        });
    }

    public void getKitesurfingSpotDetails(final KitesurfingSpotIdBodyParam id, @NonNull final OnKitesurfingSpotDetailsCallback callback) {
        apiInterface.doGetKitesurfingSpotDetails(id).enqueue(new Callback<KitesurfingSpotDetailsResp>() {
            @Override
            public void onResponse(Call<KitesurfingSpotDetailsResp> call, Response<KitesurfingSpotDetailsResp> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        GetKitesurfingSpotDetailsError error = (GetKitesurfingSpotDetailsError) APIClient.getClient()
                                .responseBodyConverter(GetKitesurfingSpotDetailsError.class, new Annotation[0])
                                .convert(response.errorBody());
                        Log.d(DATA_PROVIDER_LOG_TAG, error.getError().getMessage() + " " + error.getError().getCode());
                        callback.onFailure(error.getError().getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<KitesurfingSpotDetailsResp> call, Throwable t) {
                Log.d(DATA_PROVIDER_LOG_TAG, t.getMessage());
                callback.onFailure(t.getMessage());
            }
        });
    }

    public void addKitesurfingSpotToFavorites(final KitesurfingSpotIdBodyParam id, final KitesurfingSpotToAndFromFavCallback callback) {
        apiInterface.doAddSpotToFavorites(id).enqueue(new Callback<KitesurfingSpotFavoriteResp>() {
            @Override
            public void onResponse(Call<KitesurfingSpotFavoriteResp> call, Response<KitesurfingSpotFavoriteResp> response) {
                try {
                    if (response.isSuccessful()) {
                        if (callback != null) callback.onSucces(response.body().getResult());
                        KitesurfingSpotDatabase.updateIsFavField(id.getSpotId(), true, false);
                    } else {
                        Converter<ResponseBody, AddKitesurfingSpotToFavError> converter =
                                APIClient.getClient().responseBodyConverter(AddKitesurfingSpotToFavError.class, new Annotation[0]);
                        AddKitesurfingSpotToFavError error = converter.convert(response.errorBody());

                        if (callback != null) callback.onError(error.getError().getMessage());
                        Log.d(DATA_PROVIDER_LOG_TAG, error.getError().getMessage() + " " + error.getError().getCode());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<KitesurfingSpotFavoriteResp> call, Throwable t) {
                try {
                    KitesurfingSpotDatabase.updateIsFavField(id.getSpotId(), true, true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void removeKitesurfingSpotFromFavorites(final KitesurfingSpotIdBodyParam id, final KitesurfingSpotToAndFromFavCallback callback) {
        apiInterface.doRemoveSpotFromFavorites(id).enqueue(new Callback<KitesurfingSpotFavoriteResp>() {
            @Override
            public void onResponse(Call<KitesurfingSpotFavoriteResp> call, Response<KitesurfingSpotFavoriteResp> response) {
                try {
                    if (response.isSuccessful()) {
                        if (callback != null) callback.onSucces(response.body().getResult());
                        KitesurfingSpotDatabase.updateIsFavField(id.getSpotId(), false, false);
                    } else {
                        Converter<ResponseBody, AddKitesurfingSpotToFavError> converter =
                                APIClient.getClient().responseBodyConverter(AddKitesurfingSpotToFavError.class, new Annotation[0]);
                        AddKitesurfingSpotToFavError error = converter.convert(response.errorBody());

                        if (callback != null) callback.onError(error.getError().getMessage());
                        Log.d(DATA_PROVIDER_LOG_TAG, error.getError().getMessage() + " " + error.getError().getCode());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<KitesurfingSpotFavoriteResp> call, Throwable t) {
                try {
                    KitesurfingSpotDatabase.updateIsFavField(id.getSpotId(), false, true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public boolean isCountryValid(String country) {
        if (kitesurfingCountries != null && kitesurfingCountries.contains(country)) {
            return true;
        } else {
            return false;
        }
    }

    public void close() {
        KitesurfingSpotDatabase.close();
    }

    private void getAuthenticationSync(final String userEmail) {
        try {
            Response<ServerAuthentication> response = apiInterface.doGetAuthenticationForUser(
                    new ServerAuthentication.Email(userEmail)).execute();
            if (response.isSuccessful()) {
                APIClient.setAccesTokenValue(response.body().getToken());
            } else {
                Converter<ResponseBody, GetAuthenticationTokenError> converter =
                        APIClient.getClient().responseBodyConverter(GetAuthenticationTokenError.class, new Annotation[0]);
                GetAuthenticationTokenError error = converter.convert(response.errorBody());
                Log.d(DATA_PROVIDER_LOG_TAG, error.getError().getMessage() + " " + error.getError().getCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(DATA_PROVIDER_LOG_TAG, e.getMessage());
        }
    }

    private void synchronizeWithServer() {
        try {
            List<KitesurfingSpot> dirtyPlaces = KitesurfingSpotDatabase.readDirtyEntries();
            for (KitesurfingSpot place : dirtyPlaces) {
                if (place.isFavorite()) {
                    apiInterface.doAddSpotToFavorites(new KitesurfingSpotIdBodyParam(place.getId()))
                            .execute();
                } else {
                    apiInterface.doRemoveSpotFromFavorites(new KitesurfingSpotIdBodyParam(place.getId()))
                            .execute();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(DATA_PROVIDER_LOG_TAG, e.getMessage());
        }
    }

    private void doSynchronizedInitializations(final String userEmail) {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());
        getAuthenticationSync(userEmail);
        synchronizeWithServer();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().build());
    }

    private void doGetKitesurfingCountriesAsync() {
        apiInterface.doGetKitesurfingCountries().enqueue(new Callback<KitesurfingCountriesResp>() {
            @Override
            public void onResponse(Call<KitesurfingCountriesResp> call, Response<KitesurfingCountriesResp> response) {
                if (response.isSuccessful()) {
                    kitesurfingCountries = response.body().getResult();
                }
            }

            @Override
            public void onFailure(Call<KitesurfingCountriesResp> call, Throwable t) {
                Log.d(DATA_PROVIDER_LOG_TAG, t.getMessage());
            }
        });
    }

}
