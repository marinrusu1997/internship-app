package com.example.kitesurfingapp.data_access_layer.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.kitesurfingapp.models.KitesurfingSpot;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class KitesurfingSpotDatabase {
    private static SQLiteDatabase readableDb = null;
    private static SQLiteDatabase writableDb = null;
    private static KitesurfingSpotDbHelper helper = null;

    private KitesurfingSpotDatabase() {}

    public static void init(Context context) {
        helper = new KitesurfingSpotDbHelper(context);
        readableDb = helper.getReadableDatabase();
        writableDb = helper.getWritableDatabase();
    }

    public static void close() {
        helper.close();
    }

    public static void write(KitesurfingSpot place) throws IOException {
        ContentValues values = new ContentValues();
        values.put(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_ID, place.getId());
        values.put(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_NAME, place.getName());
        values.put(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_COUNTRY, place.getCountry());
        values.put(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_WHEN_TO_GO, place.getWhenToGo());
        values.put(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_IS_FAVORITE,
                place.getIsFavorite() ? 1 : 0);
        values.put(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_IS_DIRTY, 0);
        if (writableDb.insert(KitesurfingSpotContract.KitesurfingSpotEntry.TABLE_NAME, null, values) == -1)
            throw new IOException("Unable to insert place with id " + place.getId());
    }

    public static void write(List<KitesurfingSpot> places) throws IOException {
        for(KitesurfingSpot place : places)
            write(place);
    }

    public static void updateIsFavField(String id, boolean isFavorite, boolean isDirty) throws IOException {
        ContentValues values = new ContentValues();
        values.put(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_IS_FAVORITE,
                isFavorite ? 1 : 0);
        if (isDirty)
            values.put(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_IS_DIRTY, 1);
        if (writableDb.update(KitesurfingSpotContract.KitesurfingSpotEntry.TABLE_NAME, values,
                KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_ID + " = ?", new String[]{id}) != 1) {
            throw new IOException("Unable to update is favorite field");
        }
    }


    private static void readAllFromCursor(Cursor cursor, List<KitesurfingSpot> places) {
        while(cursor.moveToNext()) {
            places.add(new KitesurfingSpot(
                    cursor.getString(cursor.getColumnIndex(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_ID)),
                    cursor.getString(cursor.getColumnIndex(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_NAME)),
                    cursor.getString(cursor.getColumnIndex(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_COUNTRY)),
                    cursor.getString(cursor.getColumnIndex(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_WHEN_TO_GO)),
                    cursor.getInt(cursor.getColumnIndex(KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_IS_FAVORITE))
                            == 0 ? false : true
            ));
        }
        cursor.close();
    }

    public static List<KitesurfingSpot> readAll() {
        List<KitesurfingSpot> places = new LinkedList<>();
        readAllFromCursor(readableDb.rawQuery("SELECT * FROM " +
                KitesurfingSpotContract.KitesurfingSpotEntry.TABLE_NAME, null), places);
        return places;
    }

    public static List<KitesurfingSpot> readDirtyEntries() {
        List<KitesurfingSpot> places = new LinkedList<>();
       readAllFromCursor(readableDb.rawQuery("SELECT * FROM " + KitesurfingSpotContract.KitesurfingSpotEntry.TABLE_NAME +
               " WHERE " + KitesurfingSpotContract.KitesurfingSpotEntry.COLUMN_NAME_IS_DIRTY + " = 1", null), places);
        return places;
    }

    public static void dropDatabase() {
        writableDb.delete(KitesurfingSpotContract.KitesurfingSpotEntry.TABLE_NAME, "1 != 2", null);
    }
}
